package com.exam.localPayments.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.exam.localPayments.models.entities.Result;
import com.exam.localPayments.models.entities.User;
import com.exam.localPayments.models.services.IUserService;

@RestController
public class UserController {

	@Autowired
	private IUserService userService;
	
	@PostMapping(value = "addUser",  consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	public Result<User> addUser(@RequestBody User user) {
		Result<User> res = new Result<>();

		try {			
			User userData = userService.save(user);
			res.setSuccess(true);
			res.setData(userData);			
		} catch (Exception e) {
			res.setSuccess(false);
		}
		
		return res;
	}
	
}
