package com.exam.localPayments.controllers;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.exam.localPayments.models.entities.CreditCard;
import com.exam.localPayments.models.entities.RatePayment;
import com.exam.localPayments.models.entities.Result;
import com.exam.localPayments.models.exceptions.DuplicatedCardException;
import com.exam.localPayments.models.exceptions.InvalidCreditCardException;
import com.exam.localPayments.models.exceptions.InvalidPaymentException;
import com.exam.localPayments.models.services.ICreditCardService;

@RestController
public class CreditCardController {
	
	@Autowired
	private ICreditCardService creditCardService;
	
	@PostMapping(value = "addCreditCard",  consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	public Result<CreditCard> addCreditCard(@RequestBody CreditCard creditCard) {
		Result<CreditCard> res = new Result<>();

		try {			
			CreditCard card = creditCardService.save(creditCard);
			res.setSuccess(true);
			res.setData(card);			
		} catch (InvalidCreditCardException e) {
			res.setSuccess(false);
			res.setMessage("Invalid credit card.");
		} catch (DuplicatedCardException e) {
			res.setSuccess(false);
			res.setMessage("The credit card alredy exist.");
		} catch (Exception e) {
			res.setSuccess(false);
		}
		
		return res;
	}

	@PostMapping(value = "getRatePayment",  consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	public Result<RatePayment> getRatePayment(@RequestBody Map<String, String> ratePaymentRequest) {
		Result<RatePayment> res = new Result<>();
		
		try {
			String cardNumber = ratePaymentRequest.get("cardNumber");
			BigDecimal amountPayment = new BigDecimal(ratePaymentRequest.get("amountPayment"));
			RatePayment ratePayment = creditCardService.getRatePayment(cardNumber, amountPayment);
			res.setSuccess(true);
			res.setData(ratePayment);	
		} catch (InvalidPaymentException e) {
			res.setSuccess(false);
			res.setMessage("Invalid payment.");
		} catch (Exception e) {
			res.setSuccess(false);
		}

		return res;
	}
}
