package com.exam.localPayments.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.exam.localPayments.models.entities.Person;
import com.exam.localPayments.models.entities.Result;
import com.exam.localPayments.models.services.IPersonService;

@RestController
public class PersonController {
	
	@Autowired
	private IPersonService personService;
	
	@PostMapping(value = "addPerson",  consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	public Result<Person> addPerson(@RequestBody Person person) {
		Result<Person> res = new Result<>();

		try {			
			Person personData = personService.save(person);
			res.setSuccess(true);
			res.setData(personData);			
		} catch (Exception e) {
			res.setSuccess(false);
		}
		
		return res;
	}
	
}
