package com.exam.localPayments.models.exceptions;

import com.exam.localPayments.models.entities.CreditCard;

public class InvalidCreditCardException extends RuntimeException {

	private static final long serialVersionUID = -1352826717853024323L;
	
	public InvalidCreditCardException(CreditCard creditCard) {
		super("Invalid creditCard entity ('" + creditCard+ "').");
	}

}
