package com.exam.localPayments.models.exceptions;

import com.exam.localPayments.models.entities.CreditCard;

public class InvalidPaymentException extends RuntimeException {

	private static final long serialVersionUID = -8113684452377907138L;

	public InvalidPaymentException(CreditCard creditCard) {
        super("Invalid Payment, the amount is greater than the maximum card limit('" + creditCard+ "').");
    }	
}
