package com.exam.localPayments.models.exceptions;

import com.exam.localPayments.models.entities.CreditCard;

public class DuplicatedCardException extends RuntimeException {

	private static final long serialVersionUID = -8930533962419735129L;
	
    public DuplicatedCardException(CreditCard creditCard) {
        super("Duplicated entity ('" + creditCard+ "').");
    }	

}
