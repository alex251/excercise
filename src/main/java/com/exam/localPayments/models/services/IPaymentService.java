package com.exam.localPayments.models.services;

import java.math.BigDecimal;

import com.exam.localPayments.models.entities.CreditCard;

public interface IPaymentService {

	public void doPayment(CreditCard creditCard, BigDecimal amountPayment);
	
}
