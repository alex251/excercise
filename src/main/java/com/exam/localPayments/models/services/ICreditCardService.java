package com.exam.localPayments.models.services;

import java.math.BigDecimal;
import java.util.List;

import com.exam.localPayments.models.entities.CreditCard;
import com.exam.localPayments.models.entities.RatePayment;

public interface ICreditCardService {
	
	CreditCard save(CreditCard creditCard); 
	
	CreditCard findCreditCard(Long id);
	
	CreditCard findCreditCard(String cardNumber);
	
	boolean isValidPayment(CreditCard creditCard, BigDecimal amountPayment);
	
	RatePayment getRatePayment(String cardNumber, BigDecimal amountPayment);

	List<CreditCard> getCreditCardByDni(String dni);

}
