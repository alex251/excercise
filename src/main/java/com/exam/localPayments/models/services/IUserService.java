package com.exam.localPayments.models.services;

import com.exam.localPayments.models.entities.User;

public interface IUserService {
	
	User save(User user);

}
