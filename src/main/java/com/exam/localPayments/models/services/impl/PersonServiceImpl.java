package com.exam.localPayments.models.services.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.exam.localPayments.models.entities.Person;
import com.exam.localPayments.models.repositories.PersonRepository;
import com.exam.localPayments.models.services.IPersonService;

@Service
public class PersonServiceImpl implements IPersonService {

    @Resource
    PersonRepository personRepository;
	
	public Person save(Person person) {
		return  personRepository.save(person);
	}
	
}
