package com.exam.localPayments.models.services.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.exam.localPayments.models.entities.User;
import com.exam.localPayments.models.repositories.UserRepository;
import com.exam.localPayments.models.services.IUserService;

@Service
public class UserServiceImpl implements IUserService{

    @Resource
    UserRepository userRepository;
	
	public User save(User user) {
		return  userRepository.save(user);
	}
	
}
