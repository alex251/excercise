package com.exam.localPayments.models.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.exam.localPayments.models.conversions.RateCreditCard;
import com.exam.localPayments.models.entities.CreditCard;
import com.exam.localPayments.models.entities.RatePayment;
import com.exam.localPayments.models.exceptions.DuplicatedCardException;
import com.exam.localPayments.models.exceptions.InvalidCreditCardException;
import com.exam.localPayments.models.exceptions.InvalidPaymentException;
import com.exam.localPayments.models.repositories.CreditCardRepository;
import com.exam.localPayments.models.services.ICreditCardService;
import com.exam.localPayments.models.validators.CreditCardValidation;

@Service
public class CreditCardServiceImpl implements ICreditCardService {

    @Resource
    CreditCardRepository creditCardRepository;
	
	public CreditCard save(CreditCard creditCard) {
		validate(creditCard);
		applyRate(creditCard);
		return creditCardRepository.save(creditCard);
	}
	
	public CreditCard findCreditCard(Long id) {
		return creditCardRepository.findById(id).orElseThrow( () -> new RuntimeException("Doesnt exists creditcard with id:" + id) );
	}
	
	public CreditCard findCreditCard(String cardNumber) {
		return creditCardRepository.findByNumber(cardNumber).stream().findFirst().orElseThrow( () -> new RuntimeException("Doesnt exists creditcard with cardNumber:" + cardNumber) );
	}
	
	public boolean isValidPayment(CreditCard creditCard, BigDecimal amountPayment) {
		return CreditCardValidation.isValidPayment(creditCard, amountPayment);
	}
	
	public RatePayment getRatePayment(String cardNumber, BigDecimal amountPayment) {
		CreditCard creditCard = findCreditCard(cardNumber);
		validatePayment(amountPayment, creditCard);		
		RatePayment ratePayment = buildRatePayment(amountPayment, creditCard);
		return ratePayment;	
	}

	public List<CreditCard> getCreditCardByDni(String dni) {
		return creditCardRepository.getCreditCardByDni(dni);
	}
	
	private void applyRate(CreditCard creditCard) {
		Double rateValue = RateCreditCard.getRateValue(creditCard.getBrandCard());
		creditCard.setRate(rateValue);		
	}

	private RatePayment buildRatePayment(BigDecimal amountPayment, CreditCard creditCard) {
		RatePayment ratePayment = new RatePayment();
		ratePayment.setBrandCard(creditCard.getBrandCard());
		ratePayment.setRateValue(creditCard.getRate());
		BigDecimal amount = amountPayment.multiply(BigDecimal.valueOf(creditCard.getRate()));
		ratePayment.setAmount(amount);
		return ratePayment;
	}

	private void validatePayment(BigDecimal amountPayment, CreditCard creditCard) {
		boolean isValidPayment = isValidPayment(creditCard, amountPayment);
		if(!isValidPayment) {
			throw new InvalidPaymentException(creditCard);
		}
	}
	
	private void validate(CreditCard creditCard) {
		boolean isValidCard = CreditCardValidation.isValid(creditCard);
		if(!isValidCard) {
			throw new InvalidCreditCardException(creditCard);
		}		
		boolean isDuplicatedCard = isDuplicated(creditCard);
		if(isDuplicatedCard) {
			throw new DuplicatedCardException(creditCard);			
		}
	}
	
	private boolean isDuplicated(CreditCard creditCard) {
		ArrayList<CreditCard> creditCards = creditCardRepository.findByNumber(creditCard.getNumber());
		return creditCards.stream().anyMatch(c -> c.getCardholder().toLowerCase().contains(creditCard.getCardholder().toLowerCase()));
	}

}
