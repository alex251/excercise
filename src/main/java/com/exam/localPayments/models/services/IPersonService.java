package com.exam.localPayments.models.services;

import com.exam.localPayments.models.entities.Person;

public interface IPersonService {
	
	Person save(Person person);
	
}
