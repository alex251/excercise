package com.exam.localPayments.models.validators;

import java.math.BigDecimal;
import java.util.Date;

import com.exam.localPayments.models.entities.CreditCard;

public class CreditCardValidation {

	public static boolean isValid(CreditCard creditCard) {
		boolean isValid = false;
		isValid = creditCard.getDueDate().before(new Date());
		return isValid;
	}
	
	public static boolean isValidPayment(CreditCard creditCard, BigDecimal amountPayment) {
		return creditCard.getLimitAmount().compareTo(amountPayment) == 1;	
	}
	
}
