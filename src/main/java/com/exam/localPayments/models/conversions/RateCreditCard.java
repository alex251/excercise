package com.exam.localPayments.models.conversions;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class RateCreditCard {
	
    private static final Supplier<Double> squaRateSupplier = () -> Double.valueOf(LocalDateTime.now().getYear() / LocalDateTime.now().getMonth().getValue());
    private static final Supplier<Double> scoRateSupplier = () -> 0.5 * LocalDateTime.now().getMonth().getValue();
    private static final Supplier<Double> pereRateSupplier = () -> 0.1 * LocalDateTime.now().getMonth().getValue();

    private static final Map<String, Supplier<Double>> MAP = new HashMap<>();

    static {
        MAP.put("SQUA", squaRateSupplier);
        MAP.put("SCO", scoRateSupplier);
        MAP.put("PERE", pereRateSupplier);
    }

    public static Double getRateValue(String brandCard){
        return MAP.get(brandCard).get();
    } 

}
