package com.exam.localPayments.models.entities;

import java.math.BigDecimal;

public class RatePayment {
	
	String brandCard;
	Double rateValue;
	BigDecimal amount;
	
	public String getBrandCard() {
		return brandCard;
	}
	public void setBrandCard(String brandCard) {
		this.brandCard = brandCard;
	}
	public Double getRateValue() {
		return rateValue;
	}
	public void setRateValue(Double rateValue) {
		this.rateValue = rateValue;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}		

}
