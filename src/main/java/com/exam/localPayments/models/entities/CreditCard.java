package com.exam.localPayments.models.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "credit_card")
public class CreditCard implements Serializable {
	
	private static final long serialVersionUID = -4974811656673369813L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String number;
	private String cardholder;	
	@ManyToOne
	@JoinColumn(name = "person_id", referencedColumnName="id")
	private Person person;
	@Column(name="limit_amount")
	private BigDecimal limitAmount;
	private Double rate;
	private String brandCard;
	@Transient
	private Date dueDate;
	
	public Long getId() {
		return id;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getCardholder() {
		return cardholder;
	}
	public void setCardholder(String cardholder) {
		this.cardholder = cardholder;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public BigDecimal getLimitAmount() {
		return limitAmount;
	}
	public void setLimitAmount(BigDecimal limitAmount) {
		this.limitAmount = limitAmount;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public String getBrandCard() {
		return brandCard;
	}
	public void setBrandCard(String brandCard) {
		this.brandCard = brandCard;
	}
	@Override
	public String toString() {
		return "CreditCard [id=" + id + ", number=" + number + "]";
	}
	
}
