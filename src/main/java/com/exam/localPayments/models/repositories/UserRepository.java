package com.exam.localPayments.models.repositories;

import org.springframework.data.repository.CrudRepository;

import com.exam.localPayments.models.entities.User;

public interface UserRepository  extends CrudRepository<User, Long>{

}
