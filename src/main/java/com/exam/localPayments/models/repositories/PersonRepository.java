package com.exam.localPayments.models.repositories;

import org.springframework.data.repository.CrudRepository;

import com.exam.localPayments.models.entities.Person;

public interface PersonRepository extends CrudRepository<Person, Long>{

}
