package com.exam.localPayments.models.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.exam.localPayments.models.entities.CreditCard;

public interface CreditCardRepository extends CrudRepository<CreditCard, Long>{
	
	public ArrayList<CreditCard>  findByNumber(String Number);
	
	@Query("FROM CreditCard WHERE person.dni = :dni")
	List<CreditCard> getCreditCardByDni(String dni);

}
