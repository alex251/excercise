package com.exam.localPayments;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LocalPaymentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(LocalPaymentsApplication.class, args);
	}

}
